const { Author } = require("../models");

exports.getAuthors = (req, res, next) => {
  Author.findAll()
    .then((authors) => {
      if (authors.length != 0) {
        res.status(200).json({
          status: true,
          message: "Authors list",
          data: { authors },
        });
      } else {
        res.status(200).json({
          status: false,
          message: "Authors empty",
        });
      }
    })
    .catch((error) => res.json(error));
};

exports.getAuthor = (req, res, next) => {
  Author.findByPk(req.params.id)
    .then((authors) => {
      if (authors) {
        res.status(200).json({
          status: true,
          message: `Authors with ID ${req.params.id} retreived`,
          data: authors,
        });
      } else {
        res.status(200).json({
          status: false,
          message: `Authors with ID ${req.params.id} not found`,
        });
      }
    })
    .catch((error) => res.json(error));
};

exports.postAuthor = (req, res) => {
  Author.create({
    name: req.body.name,
    birth_date: req.body.birth_date,
  })
    .then((author) => {
      res.status(201).json({
        status: true,
        message: "Author created",
        data: author,
      });
    })
    .catch((error) => res.json(error));
};

exports.putAuthor = (req, res) => {
  Author.findByPk(req.params.id)
    .then((author) => {
      author
        .update(
          {
            name: req.body.name,
            birth_date: req.body.birth_date,
          },
          {
            where: {
              id: req.params.id,
            },
          }
        )
        .then(() => {
          res.status(200).json({
            status: true,
            message: `Author with ID ${req.params.id} updated`,
            data: author,
          });
        });
    })
    .catch((error) => res.json(error));
};

exports.deleteAuthor = (req, res) => {
  Author.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then(() => {
      res.status(204).json({
        status: true,
        message: `Author with ID ${req.params.id} deleted`,
      });
    })
    .catch((error) => res.json(error));
};
