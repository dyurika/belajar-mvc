const { Book } = require("../models");

exports.getBooks = (req, res, next) => {
  Book.findAll()
    .then((books) => {
      if (books.length != 0) {
        res.status(200).json({
          status: true,
          message: "Books list",
          data: { books },
        });
      } else {
        res.status(200).json({
          status: false,
          message: "Books empty",
        });
      }
    })
    .catch((error) => res.json(error));
};

exports.getBook = (req, res, next) => {
  Book.findByPk(req.params.id)
    .then((books) => {
      if (books) {
        res.status(200).json({
          status: true,
          message: `Books with ID ${req.params.id} retreived!`,
          data: books,
        });
      } else {
        res.status(200).json({
          status: false,
          message: `Books with ID ${req.params.id} not found!`,
        });
      }
    })
    .catch((error) => res.json(error));
};

exports.postBook = (req, res) => {
  Book.create({
    title: req.body.title,
    publish_date: req.body.publish_date,
    author_id: req.body.author_id,
    description: req.body.description,
  })
    .then((book) => {
      res.status(201).json({
        status: true,
        message: "Book created!",
        data: book,
      });
    })
    .catch((error) => res.json(error));
};

exports.putBook = (req, res) => {
  Book.findByPk(req.params.id)
    .then((book) => {
      book
        .update(
          {
            title: req.body.title,
            publish_date: req.body.publish_date,
            author_id: req.body.author_id,
            description: req.body.description,
          },
          {
            where: {
              id: req.params.id,
            },
          }
        )
        .then(() => {
          res.status(200).json({
            status: true,
            message: `Book with ID ${req.params.id} updated!`,
            data: book,
          });
        });
    })
    .catch((error) => res.json(error));
};

exports.deleteBook = (req, res) => {
  Book.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then(() => {
      res.status(204).json({
        status: true,
        message: `Book with ID ${req.params.id} deleted!`,
      });
    })
    .catch((error) => res.json(error));
};
