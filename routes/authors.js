var express = require("express");
var router = express.Router();

const authorController = require("../controllers/authorController");

router.get("/getAuthors", authorController.getAuthors);
router.get("/", authorController.getAuthors);
router.get("/:id", authorController.getAuthor);
router.post("/", authorController.postAuthor);
router.put("/:id", authorController.putAuthor);
router.delete("/:id", authorController.deleteAuthor);
module.exports = router;
